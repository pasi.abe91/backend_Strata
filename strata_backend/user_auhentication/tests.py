from rest_framework.test import APITestCase
from .views import get_randomnum, get_access_token, get_refresh_token
from .models import CustomUserAuth 
from .views import RegisterView
# UserProfile
# from message_control.tests import create_image, SimpleUploadedFile


class TestGenericFunctions(APITestCase):
    
    def test_get_random(self):
        
        rand1 = get_randomnum(10)
        rand2 = get_randomnum(10)
        rand3 = get_randomnum(15)
        
        
        # self.assertEqual(rand1, rand2)
        self.assertTrue(rand1)
        # self.assertEqual(len(rand1), 15)
        
    def test_get_access_token(self):
        payload = {"user_id": 2}
        
        access = get_access_token(payload)
        
        self.assertTrue(access)
        

class TestuserAuth(APITestCase):
    login_url = "/user/login"
    register_url = "/user/register"
    refresh_url = "/user/refresh"
    
    
    def test_register_user(self):
        data = {
                "username" : "SAmi",
                "email" : "amSi@gmail.com",
                "password" : "L@gi0000"
}
        
        response = self.client.post(self.register_url, data=data)

        # check that we obtain a status of 201
        self.assertEqual(response.status_code, 201)
        
        
    def test_double_register_user(self):
        data = {
                "username" : "SAmi",
                "email" : "amSi@gmail.com",
                "password" : "L@gi0000"
}
        
        response1 = self.client.post(self.register_url, data=data)
        response2 = self.client.post(self.register_url, data=data)

        # check that we obtain a status of 201
        self.assertEqual(response1.status_code, 201)
        
        # check that we obtain a status of 409 as existing user
        self.assertEqual(response2.status_code, 409)
        
        
    def test_login_user(self):
        validdata = {
            "username": "testuser",
            "password": "test123",
            "email": "xs@oo.com"
        }

        # register
        self.client.post(self.register_url, data=validdata)

        # login
        response = self.client.post(self.login_url, data=validdata)
        result_data = response.json()

        # check that we obtain a status of 200
        self.assertEqual(response.status_code, 200)

        # check that we obtained both the refresh and access token
        self.assertTrue(result_data["access"])
        self.assertTrue(result_data["refresh"])
        
    def test_login_user_invalid(self):
        validdata = {
            "username": "testuser",
            "password": "test123",
            "email": "xs@oo.com"
        }
        
        invaliddata = {
            "username": "testuserincorrect",
            "password": "test123",
            "email": "xs@oo.com"
        }

        # register
        self.client.post(self.register_url, data=validdata)
        

        # login
        response = self.client.post(self.login_url, data=invaliddata)
        result_data = response.json()

        # check that we obtain a status of 400 for inconnect login
        self.assertEqual(response.status_code, 400)
        
        
    def test_refresh(self):
        payload = {
            "username": "testuserincorrect",
            "password": "test123",
            "email": "xs@oo.com"
        }

        # register
        self.client.post(self.register_url, data=payload)

        # login
        response = self.client.post(self.login_url, data=payload)
        refresh = response.json()["refresh"]

        # get refresh
        response = self.client.post(
            self.refresh_url, data={"refresh": refresh})
        result = response.json()

        # check that we obtain a status of 200
        self.assertEqual(response.status_code, 200)

        # check that we obtained both the refresh and access token
        self.assertTrue(result["access"])
        self.assertTrue(result["refresh"])

from django.apps import AppConfig


class UserAuhenticationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'user_auhentication'

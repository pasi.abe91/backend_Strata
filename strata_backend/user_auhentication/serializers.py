from rest_framework import serializers
from .models import  CustomUserAuth, UserProfile
from django.db.models import Q
# ,Favorite
from chat_control.serializers import GenFileUploadSerializer


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()


class RegisterSerializer(serializers.Serializer):
    email = serializers.EmailField()
    username = serializers.CharField()
    password = serializers.CharField()


class RefreshSerializer(serializers.Serializer):
    refresh = serializers.CharField()


class CustomUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomUserAuth
        exclude = ("password","is_superuser", "is_staff","is_active")
        
        
class UserProfileSerializer(serializers.ModelSerializer):
    user = CustomUserSerializer(read_only=True)
    user_id = serializers.IntegerField(write_only=True)
    profile_picture = GenFileUploadSerializer(read_only=True)
    profile_picture_id = serializers.IntegerField(write_only=True, required=False)
    message_count = serializers.SerializerMethodField("get_message_count")
    class Meta:
        model = UserProfile
        fields = '__all__'

    def get_message_count(self, obj):
        try:
            user_id = self.context["request"].user.id
        except Exception as e:
            user_id = None

        from chat_control.models import Message
        # message = Message.objects.filter(sender_id=obj.user.id, receiver_id=user_id, is_read=False).distinct()
        message = Message.objects.filter(Q(sender_id=user_id, receiver_id=obj.user.id) | Q (sender_id = obj.user.id, receiver_id= user_id)).distinct()

        return message.count()

# class UserProfileSerializer(serializers.ModelSerializer):
#     user = CustomUserSerializer(read_only=True)
#     user_id = serializers.IntegerField(write_only=True)
#     profile_picture = GenericFileUploadSerializer(read_only=True)
#     profile_picture_id = serializers.IntegerField(
#         write_only=True, required=False)
#     message_count = serializers.SerializerMethodField("get_message_count")

#     class Meta: 
#         model = UserProfile
#         fields = "__all__"




class FavoriteSerializer(serializers.Serializer):
    favorite_id = serializers.IntegerField()
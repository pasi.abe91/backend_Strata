from rest_framework import serializers
from .models import GenFileUpload, Message, MessageAttachment



class GenFileUploadSerializer(serializers.ModelSerializer):

    class Meta:
        model = GenFileUpload
        fields = "__all__"

class MessageAttachmentSerializer(serializers.ModelSerializer):
    attachment = GenFileUploadSerializer()

    class Meta:
        model = MessageAttachment
        fields = "__all__"


# All related serializers must be created and initiaded before this, example :  MessageAttachmentSerializer,UserProfileSerializer       
class MessageSerializer(serializers.ModelSerializer):
    sender = serializers.SerializerMethodField('get_user_sender')
    sender_id = serializers.IntegerField(write_only=True)
    receiver = serializers.SerializerMethodField('get_user_receiver')
    receiver_id = serializers.IntegerField(write_only=True)
    message_attachments = MessageAttachmentSerializer(read_only=True, many=True)
    class Meta:
        model = Message
        fields = "__all__"
        
    # refering using reverse relationship    
    def get_user_sender(self, obj):
        
        from user_auhentication.serializers import UserProfileSerializer, CustomUserSerializer
        # return CustomUserSerializer(obj.sender.user_profile)
        return UserProfileSerializer(obj.sender.user_profile).data
    
    def get_user_receiver(self, obj):
        
        from user_auhentication.serializers import UserProfileSerializer, CustomUserSerializer
        # return CustomUserSerializer(obj.receiver.user_profile)
        return UserProfileSerializer(obj.receiver.user_profile).data
        


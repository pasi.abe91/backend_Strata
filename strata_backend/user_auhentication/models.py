from django.db import models
# from gdstorage.storage import GoogleDriveStorage

# Create your models here.

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.utils import timezone
from chat_control.models import GenFileUpload

# Define Google Drive Storage
# gd_storage = GoogleDriveStorage()
class CustomUserManager(BaseUserManager):
    
    # def get_by_natural_key(self, username):
    #     case_insensitive_username_field = '{}__iexact'.format(self.model.USERNAME_FIELD)
    #     return self.get(**{case_insensitive_username_field: username})


    def create_user(self, username, password, **extra_fields):
        if not username:
            raise ValueError("Username field is required")

        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")

        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self.create_user(username, password, **extra_fields)



class CustomUserAuth(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(unique=True, max_length=100)
    email = models.EmailField(unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_online = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = "username"
    objects = CustomUserManager()

    def __str__(self):
        return self.username

    class Meta:
        ordering = ("created_at",)
        # to chnage the name representation in Admin portal
        verbose_name = 'CustomUserAuth'
        verbose_name_plural = 'CustomUserAuth'
        
        
class Jwt(models.Model):
    user = models.OneToOneField(
        CustomUserAuth, related_name="login_user", on_delete=models.CASCADE)
    access = models.TextField()
    refresh = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    class Meta:
        ordering = ("created_at",)
   

class UserProfile(models.Model):
    # user = models.OneToOneField(CustomUserAuth, related_name="user_profile", on_delete=models.CASCADE)
    user = models.OneToOneField(CustomUserAuth, related_name="user_profile", on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50, blank=True, null=True)
    last_name = models.CharField(max_length=50, blank=True, null=True)
    caption = models.CharField(max_length=50, blank=True, null=True)
    about = models.TextField()
    profile_picture = models.ForeignKey(GenFileUpload, related_name="profile_picture", on_delete=models.SET_NULL , null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    
    def __str__(self):
        return self.user.username
    
    class Meta:
        ordering = ["updated_at"]
        
        
# Define Google Drive Storage



# class Map(models.Model):
#     id = models.AutoField( primary_key=True)
#     map_name = models.CharField(max_length=200)
#     map_data = models.FileField(upload_to='maps', storage=gd_storage)
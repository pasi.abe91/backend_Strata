from re import search
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUserAuth,UserProfile

# Register your models here.

@admin.register(CustomUserAuth)
class CustomUserAuthAdmin(UserAdmin):
    list_display = ('id','email','username','created_at','updated_at','is_superuser','is_staff','is_online')
    search_fields = ('email','username')
    readonly_fields = ('id','created_at','updated_at','is_online')
    
    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()
    
admin.site.register(( UserProfile))
